(defgroup chedr ()
  "Customization relating to CHEDR.")

(defcustom chedr-core-directory "~/chedr-core"
  "The directory where chedr-core is cloned."
  :type 'string)

(defvar *environments* '("prod" "preprod" "cert" "dev"))

(defun chedr-port-forward (environment database port)
  "In a backround proceess, connect to one of the chedr environments."
  (interactive
   (let (environment)
     (list
      (setq environment
            (concat
             "cx-"
             (completing-read-default
              "Environment: " *environments*
              (lambda (in) (member in *environments*))
              t nil nil nil t)))
      (read-string "Database: " "charcuteriedb")
      (read-string "Port: "
                   (cond
                    ((string= "cx-prod" environment) "54324")
                    ((string= "cx-preprod" environment)"54323")
                    ((string= "cx-cert" environment) "54322")
                    ((string= "cx-dev" environment) "54321"))))))
  (start-process
   (concat environment "-port-forward")
   (concat "*Port Forward: <" environment ">*")
   (concat chedr-core-directory
           "/scripts/db/cloudsql-port-forward")
   "--cluster" environment
   "--database" database
   "--port" port))

(provide 'chedr)
